﻿using System.Collections.Generic;
using UnityEngine;
using EcoSystem.Models;

namespace EcoSystem.Controllers
{
	public class EcoSystemController : MonoBehaviour
	{
		#region Members

		public static Models.EcoSystem ActiveEcoSystem;
		public static EcoSystemController Instance;
		public static List<Models.EcoSystem> _EcoSystems = new List<Models.EcoSystem>();

		private List<AnimalType> _AnimalsList = new List<AnimalType>();

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region MonoBehaviour

		private void Awake()
		{
			if (Instance == null)
			{
				Instance = this;
			}
		}

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region Public

		public void CreateEcoSystem()
		{
			List<Animal> animals = new List<Animal>();
			Models.EcoSystem ecoSystem = new Models.EcoSystem();
			_EcoSystems.Add(ecoSystem);
			ActiveEcoSystem = ecoSystem;
		}

		public void AddAnimalToList(AnimalType animalType)
		{
			_AnimalsList.Add(animalType);
		}

		public List<AnimalType> GetAnimalsList()
		{
			return _AnimalsList;
		}

		public void AddAnimal(Animal animal)
		{
			ActiveEcoSystem.LivingAnimals.Add(animal);
		}

		public void RemoveAnimal(Animal animal)
		{
			ActiveEcoSystem.LivingAnimals.Remove(animal);
			ActiveEcoSystem.DeadAnimals.Add(animal);
		}

		#endregion
	}
}