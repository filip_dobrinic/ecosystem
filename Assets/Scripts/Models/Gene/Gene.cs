﻿using EcoSystem.Utility;

namespace EcoSystem.Models
{
	#region Enum

	public enum GeneType
	{
		Endurance,
		Speed,
		Strength
	}

	#endregion
	//----------------------------------------------------------------------------------------------------------------------------
	#region Public

	public class Gene
	{
		private const float MUTATION_DEVIATION = 0.05f;

		public float Value;
		public float HeredityWeight;

		public GeneType Type { get; private set; }

		public Gene(NormalizedGeneValueRange range, GeneType type)
		{
			Value = EcoSystemUtility.GetRandomFloat(range.MinValue, range.MaxValue);
			HeredityWeight = EcoSystemUtility.GetRandomFloat(0f, 1f);
			Type = type;
		}

		public Gene(Gene firstGene, Gene secondGene)
		{
			Gene lowerValueGene, higherValueGene;
			if (firstGene.Value <= secondGene.Value)
			{
				lowerValueGene = firstGene;
				higherValueGene = secondGene;
			}
			else
			{
				lowerValueGene = secondGene;
				higherValueGene = firstGene;
			}
			Type = firstGene.Type;
			Value = EcoSystemUtility.GetClampedValue((lowerValueGene.Value * lowerValueGene.HeredityWeight + higherValueGene.Value * higherValueGene.HeredityWeight) * EcoSystemUtility.GetRandomFloat(1f - MUTATION_DEVIATION, 1f + MUTATION_DEVIATION),
				lowerValueGene.Value - MUTATION_DEVIATION,
				higherValueGene.Value + MUTATION_DEVIATION);
			HeredityWeight = EcoSystemUtility.GetRandomFloat(0f, 1f);
		}
	}

	#endregion
}