﻿using System.Collections.Generic;

namespace EcoSystem.Models
{
	public class EcoSystem
	{
		#region Members

		public List<Animal> LivingAnimals;
		public List<Animal> DeadAnimals;

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region Public

		public EcoSystem()
		{
			LivingAnimals = new List<Animal>();
			DeadAnimals = new List<Animal>();
		}

		public EcoSystem(List<Animal> initialAnimals)
		{
			LivingAnimals = initialAnimals;
		}

		#endregion
	}
}
