﻿namespace EcoSystem.Models
{
	public class Rabbit : Animal
	{
		#region Members

		private const float ENDURANCE_MIN_VALUE = 0.85f;
		private const float ENDURANCE_MAX_VALUE = 1f;
		private const float SPEED_MIN_VALUE = 0.9f;
		private const float SPEED_MAX_VALUE = 1f;
		private const float STRENGTH_MIN_VALUE = 0.05f;
		private const float STRENGTH_MAX_VALUE = 0.15f;

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region Public

		public Rabbit() : base()
		{
		}

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region Private

		protected override void _FillGenesList()
		{
			Genes.Add(new Gene(new NormalizedGeneValueRange { MinValue = ENDURANCE_MIN_VALUE, MaxValue = ENDURANCE_MAX_VALUE }, GeneType.Endurance));
			Genes.Add(new Gene(new NormalizedGeneValueRange { MinValue = SPEED_MIN_VALUE, MaxValue = SPEED_MAX_VALUE }, GeneType.Speed));
			Genes.Add(new Gene(new NormalizedGeneValueRange { MinValue = STRENGTH_MIN_VALUE, MaxValue = STRENGTH_MAX_VALUE }, GeneType.Strength));
		}

		#endregion
	}
}
