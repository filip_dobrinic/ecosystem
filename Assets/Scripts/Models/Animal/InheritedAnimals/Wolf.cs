﻿using System.Collections.Generic;

namespace EcoSystem.Models
{
	public class Wolf : Animal
	{
		#region Members

		private const float ENDURANCE_MIN_VALUE = 0.75f;
		private const float ENDURANCE_MAX_VALUE = 0.9f;
		private const float SPEED_MIN_VALUE = 0.7f;
		private const float SPEED_MAX_VALUE = 0.8f;
		private const float STRENGTH_MIN_VALUE = 0.55f;
		private const float STRENGTH_MAX_VALUE = 0.75f;

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region Public

		public Wolf() : base()
		{
		}

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region Private

		protected override void _FillFoodChainList()
		{
			FoodChain = new List<AnimalType> { AnimalType.Fox, AnimalType.Rabbit, AnimalType.Lynx };
		}

		protected override void _FillGenesList()
		{
			Genes.Add(new Gene(new NormalizedGeneValueRange { MinValue = ENDURANCE_MIN_VALUE, MaxValue = ENDURANCE_MAX_VALUE }, GeneType.Endurance));
			Genes.Add(new Gene(new NormalizedGeneValueRange { MinValue = SPEED_MIN_VALUE, MaxValue = SPEED_MAX_VALUE }, GeneType.Speed));
			Genes.Add(new Gene(new NormalizedGeneValueRange { MinValue = STRENGTH_MIN_VALUE, MaxValue = STRENGTH_MAX_VALUE }, GeneType.Strength));
		}

		#endregion
	}
}
