﻿namespace EcoSystem.Models
{
	#region Public

	/// <summary>
	/// Model to View bridge
	/// </summary>
	public enum AnimalType
	{
		None,
		Bear,
		Fox,
		Lynx,
		Rabbit,
		Wolf
	}

	public enum AnimalSex
	{
		Male,
		Female
	}

	public interface IAnimal
	{
		void ModifyHunger(float amount);
		void ModifyThirst(float amount);
		void ModifyReproductionUrge(float amount);
		void ModifyHealth(float amount);
	}

	#endregion
}
