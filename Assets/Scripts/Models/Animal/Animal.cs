﻿using System;
using System.Collections.Generic;
using EcoSystem.Utility;

namespace EcoSystem.Models
{
	public class Animal : IAnimal
	{
		#region Members

		private const float MIN_GENE_VALUE = 0f;
		private const float MAX_GENE_VALUE = 1f;
		private const int GENE_COUNT = 3;

		protected Dictionary<Gene, NormalizedGeneValueRange> _PredifinedGeneData;

		public List<AnimalType> FoodChain;
		public AnimalSex AnimalSex;
		public List<Gene> Genes = new List<Gene>();
		public float Thirst;
		public float Hunger;
		public float ReproductionUrge;
		public float Health;

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region Public

		public Animal()
		{
			Random rnd = new Random(Guid.NewGuid().GetHashCode());
			AnimalSex = (AnimalSex)rnd.Next(0, 2);
			_FillGenesList();
			_SetupInitialStats();
			_FillFoodChainList();
		}

		public Animal(Animal firstAnimal, Animal secondAnimal)
		{
			for (int i = 0; i < GENE_COUNT; ++i)
			{
				Genes.Add(new Gene(firstAnimal.Genes[i], secondAnimal.Genes[i]));
			}
			_SetupInitialStats();
			_FillFoodChainList();
			FoodChain = firstAnimal.FoodChain;
		}

		public void ModifyHunger(float amount)
		{
			Hunger = EcoSystemUtility.GetClampedValue(Hunger + amount, 0, Const.ONE_HUNDRED);
		}

		public void ModifyThirst(float amount)
		{
			Thirst = EcoSystemUtility.GetClampedValue(Thirst + amount, 0, Const.ONE_HUNDRED);
		}

		public void ModifyReproductionUrge(float amount)
		{
			ReproductionUrge = EcoSystemUtility.GetClampedValue(ReproductionUrge + amount, 0, Const.ONE_HUNDRED);
		}

		public void ModifyHealth(float amount)
		{
			Health = EcoSystemUtility.GetClampedValue(Health + amount, 0, Const.ONE_HUNDRED);
		}

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region Private

		private void _SetupInitialStats()
		{
			Thirst = 50f;
			Hunger = 50f;
			ReproductionUrge = 20f;
			Health = 100f;
		}

		protected virtual void _FillFoodChainList()
		{
			FoodChain = new List<AnimalType> { AnimalType.None };
		}

		protected virtual void _FillGenesList()
		{
			Genes.Add(new Gene(new NormalizedGeneValueRange { MinValue = MIN_GENE_VALUE, MaxValue = MAX_GENE_VALUE }, GeneType.Endurance));
			Genes.Add(new Gene(new NormalizedGeneValueRange { MinValue = MIN_GENE_VALUE, MaxValue = MAX_GENE_VALUE }, GeneType.Speed));
			Genes.Add(new Gene(new NormalizedGeneValueRange { MinValue = MIN_GENE_VALUE, MaxValue = MAX_GENE_VALUE }, GeneType.Strength));
		}

		#endregion
	}
}
