﻿namespace EcoSystem.Models
{
	public struct NormalizedGeneValueRange
	{
		public float MinValue { get; set; }
		public float MaxValue { get; set; }
	}
}