﻿using System;
using EcoSystem.Models;

namespace EcoSystem.Utility
{
	public class EcoSystemUtility
	{
		public static Animal GetAnimalByType(AnimalType type)
		{
			switch (type)
			{
				case AnimalType.None:
					return new Animal();
				case AnimalType.Bear:
					return new Bear();
				case AnimalType.Fox:
					return new Fox();
				case AnimalType.Lynx:
					return new Lynx();
				case AnimalType.Rabbit:
					return new Rabbit();
				case AnimalType.Wolf:
					return new Wolf();
				default:
					return new Animal();
			}
		}

		public static Type GetAnimalClassType(AnimalType type)
		{
			switch (type)
			{
				case AnimalType.None:
					return typeof(Animal);
				case AnimalType.Bear:
					return typeof(Bear);
				case AnimalType.Fox:
					return typeof(Fox);
				case AnimalType.Lynx:
					return typeof(Lynx);
				case AnimalType.Rabbit:
					return typeof(Rabbit);
				case AnimalType.Wolf:
					return typeof(Wolf);
				default:
					return typeof(Animal);
			}
		}

		public static float GetRandomFloat(float min, float max)
		{
			Random rand = new Random(Guid.NewGuid().GetHashCode());
			int tempInt = rand.Next((int)(min * 100f), (int)(max * 100f));
			return tempInt / 100f;
		}

		public static float GetClampedValue(float value, float min, float max)
		{
			return (value < min) ? min : (value > max) ? max : value;  
		}
	}
}