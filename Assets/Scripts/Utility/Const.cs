﻿namespace EcoSystem.Utility
{
	public class Const
	{
		//Digits
		public static char MIN_SINGLE_DIGIT = '0';
		public static char MAX_SINGLE_DIGIT = '9';

		//Animal numbers
		public static int MIN_ANIMAL_INIT_NUMBER = 30;
		public static int MAX_ANIMAL_INIT_NUMBER = 50;

		//Operation numbers
		public static float ONE_HUNDRED = 100f;
		public static float TEN = 10f;
	}
}
