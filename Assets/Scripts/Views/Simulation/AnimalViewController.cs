﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using EcoSystem.Models;
using EcoSystem.Utility;
using EcoSystem.Controllers;

namespace EcoSystem.Views
{
	enum ActionType
	{
		Reproduction = 1,
		Thirst = 2,
		Hunger = 3,
		Run = 4
	}

	public class AnimalViewController : MonoBehaviour
	{
		#region Members

		private const float UPPER_DESIRE_LIMIT = 80f;
		private const float LOWER_DESIRE_LIMIT = 30f;
		private const float REPRODUCTION_URGE_LIMIT = 70f;
		private const float INTERACTION_DISTANCE = 3f;

		public Animal CorrespondingAnimal;
		public AnimalType Type;
		public NavMeshAgent Agent;

		private List<GameObject> _FoodChainList;
		private List<ActionType> _ActionsList = new List<ActionType>();
		private GameObject _Target;
		private SimulationViewController _SimulationViewController;
		private float _HungerIncreaseAmount;
		private float _ThirstIncreaseAmount;
		private float _ReproductionUrgeIncreaseAmount;
		private float _DamageAmount;
		private float _UpdatePeriod;
		private float _Timer;
		private bool _CanAttack;

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region MonoBehaviour

		private void Awake()
		{
			_SimulationViewController = SimulationViewController.Instance;
			_SimulationViewController.AddAnimalObjectToList(gameObject);
		}

		private void Start()
		{
			_SetupInitialStats();
		}

		private void Update()
		{
			_Timer += Time.deltaTime;

			if (_Timer >= _UpdatePeriod)
			{
				_Timer = 0f;
				_UpdateAnimalValues();
				_CheckForActions();
			}
			if (_ActionsList.Count != 0)
			{
				_ActionsList = _ActionsList.OrderByDescending(actionType => (int)actionType).ToList();
				switch (_ActionsList[0])
				{
					case ActionType.Reproduction:
						_ReproductionAction();
						break;
					case ActionType.Thirst:
						_ThirstAction();
						break;
					case ActionType.Hunger:
						_HungerAction();
						break;
					case ActionType.Run:
						_RunAction();
						break;
					default:
						break;
				}
			}
			else
			{
				Agent.SetDestination(transform.position);
			}
		}

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region Public

		public void TakeDamage(float amount)
		{
			CorrespondingAnimal.ModifyHealth(-amount);
			if (CorrespondingAnimal.Health <= 0f)
			{
				_Die();
			}
		}

		private void AddRunAction()
		{
			if (!_ActionsList.Contains(ActionType.Run))
			{
				_ActionsList.Add(ActionType.Run);
			}
		}

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region Private

		private void _SetupInitialStats()
		{
			float endurance = CorrespondingAnimal.Genes[0].Value;
			float speed = CorrespondingAnimal.Genes[1].Value;
			float strength = CorrespondingAnimal.Genes[2].Value;
			_HungerIncreaseAmount = (strength / endurance) * 2f + 1f;
			_ThirstIncreaseAmount = (strength / endurance) * 2f;
			_DamageAmount = strength * Const.ONE_HUNDRED;
			_UpdatePeriod = endurance + 2f;
			_ReproductionUrgeIncreaseAmount = (speed + endurance) * 3f;
			Agent.speed = speed * Const.TEN;
		}

		private void _UpdateAnimalValues()
		{
			_CanAttack = true;
			CorrespondingAnimal.ModifyThirst(_ThirstIncreaseAmount);
			CorrespondingAnimal.ModifyHunger(_HungerIncreaseAmount);
			CorrespondingAnimal.ModifyReproductionUrge(_ReproductionUrgeIncreaseAmount);
		}

		private void _CheckForActions()
		{
			if (CorrespondingAnimal.Thirst > UPPER_DESIRE_LIMIT)
			{
				TakeDamage(Const.TEN);
			}
			if (CorrespondingAnimal.Hunger > UPPER_DESIRE_LIMIT)
			{
				TakeDamage(Const.TEN);
			}
			if (!_ActionsList.Contains(ActionType.Hunger) && CorrespondingAnimal.Hunger > LOWER_DESIRE_LIMIT)
			{
				_ActionsList.Add(ActionType.Hunger);
			}
			if (!_ActionsList.Contains(ActionType.Thirst) && CorrespondingAnimal.Thirst > LOWER_DESIRE_LIMIT)
			{
				_ActionsList.Add(ActionType.Thirst);
			}
			if (!_ActionsList.Contains(ActionType.Reproduction) && CorrespondingAnimal.ReproductionUrge > REPRODUCTION_URGE_LIMIT && CorrespondingAnimal.Hunger < LOWER_DESIRE_LIMIT && CorrespondingAnimal.Thirst < LOWER_DESIRE_LIMIT)
			{
				_ActionsList.Add(ActionType.Reproduction);
			}
		}

		private void _HungerAction()
		{
			_UpdateFoodChainList();
			_Target = _FoodChainList.OrderBy(animal => Vector3.Distance(animal.transform.position, gameObject.transform.position)).FirstOrDefault();
			if (_Target == null)
			{
				_Target = _SimulationViewController.FoodList.OrderBy(food => Vector3.Distance(gameObject.transform.position, food.transform.position)).FirstOrDefault();
				if (_Target == null)
				{
					return;
				}
			}

			AnimalViewController targetAnimalController = _Target.GetComponent<AnimalViewController>();
			if (targetAnimalController != null)
			{
				targetAnimalController.AddRunAction();
			}

			_SetDestinationToTarget();
			if (_IsDistanceCloseEnough())
			{
				if (_AttackOtherAnimalOrPlant(_Target.GetComponent<AnimalViewController>()) == true)
				{
					_ActionsList.Remove(ActionType.Hunger);
					_Target = null;
				}
			}
		}

		private void _ThirstAction()
		{
			_Target = _SimulationViewController.WaterSourcesList.OrderBy(waterSource => Vector3.Distance(waterSource.transform.position, gameObject.transform.position)).FirstOrDefault();
			if (_Target != null)
			{
				_SetDestinationToTarget();
				if (_IsDistanceCloseEnough())
				{
					CorrespondingAnimal.ModifyThirst(-Const.ONE_HUNDRED);
					CorrespondingAnimal.ModifyHealth(Const.TEN);
					_ActionsList.Remove(ActionType.Thirst);
					_Target = null;
				}
			}
		}

		private void _ReproductionAction()
		{
			if (_Target == null)
			{
				List<GameObject> ValidPartners = new List<GameObject>();
				foreach (GameObject animal in _SimulationViewController.AnimalObjectsList)
				{
					AnimalViewController animalView = animal.GetComponent<AnimalViewController>();
					if (animalView.Type.Equals(Type) && animalView.CorrespondingAnimal.AnimalSex != CorrespondingAnimal.AnimalSex)
					{
						ValidPartners.Add(animal);
					}
				}
				_Target = ValidPartners.OrderBy(animal => Vector3.Distance(animal.transform.position, gameObject.transform.position)).FirstOrDefault();
				if (_Target == null)
				{
					_ActionsList.Remove(ActionType.Reproduction);
				}
			}
			else
			{
				_SetDestinationToTarget();
				if (_IsDistanceCloseEnough())
				{
					_SpawnChildren(_Target.GetComponent<AnimalViewController>());
					_ActionsList.Remove(ActionType.Reproduction);
					_Target = null;
				}
			}
		}

		private void _RunAction()
		{
			if (_Target == null)
			{
				_Target = _SimulationViewController.RunningPoints.OrderByDescending(runningPoint => Vector3.Distance(runningPoint.transform.position, gameObject.transform.position)).ToArray()[1];
			}
			else
			{
				_SetDestinationToTarget();
				if (_IsDistanceCloseEnough())
				{
					_Target = null;
					_ActionsList.Remove(ActionType.Run);
				}
			}
		}

		private void _SetDestinationToTarget()
		{
			if (Agent.isOnNavMesh)
			{
				Agent.SetDestination(_Target.transform.position);
			}
		}

		private bool _IsDistanceCloseEnough()
		{
			return Vector3.Distance(_Target.transform.position, gameObject.transform.position) < INTERACTION_DISTANCE;
		}

		private bool _AttackOtherAnimalOrPlant(AnimalViewController animalController)
		{
			if (!_CanAttack)
			{
				return false;
			}

			if (animalController == null)
			{
				_SimulationViewController.FoodList.Remove(_Target);
				Destroy(_Target);
				CorrespondingAnimal.ModifyHunger(-Const.TEN);
				_CanAttack = false;
				return true;
			}
			else
			{
				if (animalController.CorrespondingAnimal.Health <= _DamageAmount)
				{
					CorrespondingAnimal.ModifyHunger(-Const.ONE_HUNDRED);
					CorrespondingAnimal.ModifyThirst(-Const.TEN);
					CorrespondingAnimal.ModifyHealth(Const.TEN);
					animalController.TakeDamage(_DamageAmount);
					_CanAttack = false;
					_Target = null;
					return true;
				}
				animalController.TakeDamage(_DamageAmount);
				CorrespondingAnimal.ModifyHunger(-Const.TEN);
				_CanAttack = false;
				_Target = null;
				return false;
			}
		}

		private void _SpawnChildren(AnimalViewController otherParent)
		{
			CorrespondingAnimal.ModifyReproductionUrge(-Const.ONE_HUNDRED);
			System.Random rnd = new System.Random();
			int iterCount = rnd.Next(1, 3);
			for (int i = 0; i < iterCount; ++i)
			{
				GameObject newAnimal = Instantiate(_SimulationViewController.AnimalsDictionary[Type], new Vector3(rnd.Next(-2, 2), 0f, rnd.Next(-2, 2)), Quaternion.identity);
				AnimalViewController controller = newAnimal.GetComponent<AnimalViewController>();
				controller.CorrespondingAnimal = new Animal(CorrespondingAnimal, otherParent.CorrespondingAnimal);
				EcoSystemController.Instance.AddAnimal(controller.CorrespondingAnimal);
			}
		}

		private void _UpdateFoodChainList()
		{
			_FoodChainList = _SimulationViewController.AnimalObjectsList.
				Where(animal => CorrespondingAnimal.FoodChain.Contains(animal.GetComponent<AnimalViewController>().Type)).
				ToList();
		}

		private void _Die()
		{
			_SimulationViewController.RemoveAnimalObjectFromList(gameObject);
			EcoSystemController.Instance.RemoveAnimal(CorrespondingAnimal);
			Destroy(gameObject);
		}

		#endregion
	}
}