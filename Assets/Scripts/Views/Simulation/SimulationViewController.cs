﻿using System;
using System.Collections.Generic;
using EcoSystem.Controllers;
using EcoSystem.Models;
using EcoSystem.Utility;
using UnityEngine;
using UnityEngine.AI;

namespace EcoSystem.Views
{
	#region Misc

	[Serializable]
	public struct AnimalObjectPair
	{
		public AnimalType Animal;
		public GameObject Prefab;
	}

	public struct AnimalInfo
	{
		public AnimalType Type;
		public List<Gene> Genes;
	}

	#endregion

	public class SimulationViewController : MonoBehaviour
	{
		#region Members

		private const float MAX_RAYCAST_DISTANCE = 200f;
		private const float FOOD_RESPAWN_TIME_REQUIRED = 5f;
		private const float GLOBAL_TIMER_PERIOD = 5f;
		private const float POSITION_LIMIT_ON_MAP = 35f;
		private const float SPAWN_HEIGHT = 50f;
		private const int MIN_FOOD_SPAWN_AMOUNT = 2;
		private const int MAX_FOOD_SPAWN_AMOUNT = 4;


		public static SimulationViewController Instance;
		[Header("GameObjects per animal type")]
		public AnimalObjectPair[] AnimalObjectPairs;
		public List<GameObject> WaterSourcesList;
		public List<GameObject> RunningPoints;
		public GameObject FoodPrefab;
		public GameObject SimulationStatsPanel;
		public GameObject EndSimulationButton;
		[HideInInspector]
		public List<GameObject> AnimalObjectsList = new List<GameObject>();
		[HideInInspector]
		public List<GameObject> FoodList = new List<GameObject>();
		public Dictionary<AnimalType, GameObject> AnimalsDictionary = new Dictionary<AnimalType, GameObject>();
		public Dictionary<int, List<AnimalInfo>> ReportDictionary = new Dictionary<int, List<AnimalInfo>>();

		private EcoSystemController _EcoSystemController;
		private float _GlobalTimer = 0f;
		private float _FoodSpawnTimer = 0f;
		private int _ReportInputIndex = 0;

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region MonoBehaviour

		private void Awake()
		{
			if (Instance == null)
			{
				Instance = this;
			}
		}

		private void Start()
		{
			_EcoSystemController = EcoSystemController.Instance;
			_InitAnimals();
			_SaveReport();
		}

		private void Update()
		{
			_GlobalTimer += Time.deltaTime;
			if (_GlobalTimer > GLOBAL_TIMER_PERIOD)
			{
				++_ReportInputIndex;
				_GlobalTimer = 0;

				_SaveReport();
			}

			_FoodSpawnTimer += Time.deltaTime;
			if (_FoodSpawnTimer > FOOD_RESPAWN_TIME_REQUIRED)
			{
				int rand = UnityEngine.Random.Range(MIN_FOOD_SPAWN_AMOUNT, MAX_FOOD_SPAWN_AMOUNT);
				for (int i = 0;  i < rand; i++)
				{
					GameObject foodObject = Instantiate(FoodPrefab, new Vector3(UnityEngine.Random.Range(-POSITION_LIMIT_ON_MAP, POSITION_LIMIT_ON_MAP), 0f, UnityEngine.Random.Range(-POSITION_LIMIT_ON_MAP, POSITION_LIMIT_ON_MAP)), Quaternion.identity);
					FoodList.Add(foodObject);
				}
				_FoodSpawnTimer = 0f;
			}
		}

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region Public

		public void AddAnimalObjectToList(GameObject obj)
		{
			AnimalObjectsList.Add(obj);
		}

		public void RemoveAnimalObjectFromList(GameObject obj)
		{
			AnimalObjectsList.Remove(obj);
		}

		public void EndSimulation()
		{
			foreach (var animal in AnimalObjectsList)
			{
				Destroy(animal);
			}
			SimulationStatsPanel.SetActive(true);
			EndSimulationButton.SetActive(false);
			Destroy(gameObject);
		}

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region Private

		private void _InitAnimals()
		{
			_ConvertStructToDictionary();
			System.Random rnd = new System.Random();
			foreach (var item in _EcoSystemController.GetAnimalsList())
			{
				Vector3 randomPosition = new Vector3(UnityEngine.Random.Range(-POSITION_LIMIT_ON_MAP, POSITION_LIMIT_ON_MAP), SPAWN_HEIGHT, UnityEngine.Random.Range(-POSITION_LIMIT_ON_MAP, POSITION_LIMIT_ON_MAP));
				randomPosition = _GetPositionOnMesh(randomPosition);
				GameObject animalObject = Instantiate(AnimalsDictionary[item], randomPosition, Quaternion.identity);
				AnimalViewController animalController = animalObject.GetComponent<AnimalViewController>();
				Animal newAnimal = EcoSystemUtility.GetAnimalByType(item);
				animalController.CorrespondingAnimal = newAnimal;
				animalController.Type = item;
				_EcoSystemController.AddAnimal(newAnimal);
				animalObject.GetComponent<NavMeshAgent>().SetDestination(_GetPositionOnMesh(new Vector3(UnityEngine.Random.Range(-POSITION_LIMIT_ON_MAP, POSITION_LIMIT_ON_MAP), 0f, UnityEngine.Random.Range(-POSITION_LIMIT_ON_MAP, POSITION_LIMIT_ON_MAP))));
			}
		}

		private void _ConvertStructToDictionary()
		{
			foreach (var item in AnimalObjectPairs)
			{
				AnimalsDictionary.Add(item.Animal, item.Prefab);
			}
		}

		private void _SaveReport()
		{
			List<AnimalInfo> infoList = new List<AnimalInfo>();
			foreach (var gameObj in AnimalObjectsList)
			{
				AnimalViewController animalView = gameObj.GetComponent<AnimalViewController>();
				infoList.Add(new AnimalInfo { Genes = animalView.CorrespondingAnimal.Genes, Type = animalView.Type });
			}
			ReportDictionary.Add(_ReportInputIndex, infoList);
		}

		private Vector3 _GetPositionOnMesh(Vector3 originalPosition)
		{
			RaycastHit hit;
			if (Physics.Raycast(originalPosition, Vector3.down, out hit, MAX_RAYCAST_DISTANCE))
			{
				return hit.point;
			}
			return Vector3.zero;
		}

		#endregion
	}
}