﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using EcoSystem.Models;

namespace EcoSystem.Views
{
	public class SimulationGraphView : MonoBehaviour
	{
		#region Members

		private const float TIME_PERIOD = 5f;
		private const float LABELS_OFFSET = 10f;

		public GameObject NodePrefab;
		public GameObject LinePrefab;
		public GameObject TextLabelPrefab;

		public List<Text> GeneTexts;
		public Text GraphTitleText;
		public RectTransform LinesParentTransform;
		public float GraphHeight;
		public float GraphWidth;

		private SimulationViewController _SimulationViewController;
		private Dictionary<int, List<AnimalInfo>> _ReportDictionary;
		private AnimalType _DisplayedType;

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region MonoBehaviour

		private void Start()
		{
			_SimulationViewController = SimulationViewController.Instance;
			_ReportDictionary = _SimulationViewController.ReportDictionary;
			DrawGraph("None");
		}

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region Public

		public void DrawGraph(string type)
		{
			GraphTitleText.text = type;
			foreach (Transform objectTransform in LinesParentTransform)
			{
				Destroy(objectTransform.gameObject);
			}

			bool parsed = Enum.TryParse(type, out _DisplayedType);
			if (parsed.Equals(false))
			{
				return;
			}

			int index = 0;
			Vector2[] nodePoints = new Vector2[_ReportDictionary.Count];
			int maxAnimalCount = _GetMaxAnimalCount();

			foreach (var keyValuePair in _ReportDictionary)
			{
				float xValue = (float)index / (_ReportDictionary.Count - 1);
				float yValue = _DisplayedType.Equals(AnimalType.None) ? ((float)keyValuePair.Value.Count / maxAnimalCount) : ((float)keyValuePair.Value.Where(info => info.Type.Equals(_DisplayedType)).Count() / maxAnimalCount);
				nodePoints[index] = _CreateNode(xValue, yValue);
				_CreateLabel((index * TIME_PERIOD).ToString(), new Vector2(nodePoints[index].x, -(GraphHeight / 2) - LABELS_OFFSET));
				_CreateLabel((yValue * maxAnimalCount).ToString(), new Vector2(-(GraphWidth / 2) - LABELS_OFFSET, (yValue - 0.5f) * GraphHeight));
				++index;
			}
			for (int i = 0; i < index - 1; i++)
			{
				_LinkNodes(nodePoints[i], nodePoints[i + 1]);
			}
			_FillGenesList(_DisplayedType);
		}

		public void QuitApplication()
		{
			Application.Quit();
		}

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region Private

		private void _FillGenesList(AnimalType type)
		{
			int index = 0;
			List<AnimalInfo> animalInfos;
			if (!_DisplayedType.Equals(AnimalType.None))
			{
				animalInfos = _ReportDictionary[index].Where(animalInfo => animalInfo.Type.Equals(_DisplayedType)).ToList();
				do
				{
					animalInfos = _ReportDictionary[index].Where(animalInfo => animalInfo.Type.Equals(_DisplayedType)).ToList();
					++index;
				}
				while (animalInfos.Count > 0 && index.Equals(_ReportDictionary.Count - 1));
				animalInfos = _ReportDictionary[index - 1].Where(animalInfo => animalInfo.Type.Equals(_DisplayedType)).ToList();
			}
			else
			{
				animalInfos = _ReportDictionary[_ReportDictionary.Count - 1];
			}
			List<Gene> allGenesList = new List<Gene>();
			foreach (var animalInfo in animalInfos)
			{
				allGenesList.AddRange(animalInfo.Genes);
			}
			List<Gene> EnduranceGenes = allGenesList.Where(gene => gene.Type.Equals(GeneType.Endurance)).ToList();
			List<Gene> StrengthGenes = allGenesList.Where(gene => gene.Type.Equals(GeneType.Strength)).ToList();
			List<Gene> SpeedGenes = allGenesList.Where(gene => gene.Type.Equals(GeneType.Speed)).ToList();
			float averageEnduranceValue = EnduranceGenes.Average(gene => gene.Value);
			float averageStrengthValue = StrengthGenes.Average(gene => gene.Value);
			float averageSpeedValue = SpeedGenes.Average(gene => gene.Value);

			Dictionary<string, float> averageGeneValues = new Dictionary<string, float>()
			{
				{ "Speed", averageSpeedValue },
				{ "Strength", averageStrengthValue },
				{ "Endurance", averageEnduranceValue }
			};

			int dictionaryIndex = 0;
			foreach (var entry in averageGeneValues.OrderByDescending(entry => entry.Value))
			{
				GeneTexts[dictionaryIndex].text = $"{entry.Key} value: {Math.Round(entry.Value, 2).ToString()}";
				++dictionaryIndex;
			}
		}

		private void _CreateLabel(string text, Vector2 position)
		{
			GameObject textLabel = Instantiate(TextLabelPrefab, LinesParentTransform);
			((RectTransform)textLabel.transform).localPosition = position;
			textLabel.GetComponent<Text>().text = text;
		}

		private int _GetMaxAnimalCount()
		{
			int maxAnimalCount = 0;

			foreach (var keyValuePair in _ReportDictionary)
			{

				int animalCount = _DisplayedType.Equals(AnimalType.None) ? keyValuePair.Value.Count : keyValuePair.Value.Where(p => p.Type.Equals(_DisplayedType)).ToList().Count;
				if (animalCount > maxAnimalCount)
				{
					maxAnimalCount = animalCount;
				}
			}
			return maxAnimalCount;
		}

		private Vector2 _CreateNode(float xPercent, float yPercent)
		{
			GameObject node = Instantiate(NodePrefab, LinesParentTransform);
			RectTransform nodeTransform = (RectTransform)node.transform;
			nodeTransform.localPosition = new Vector2((xPercent - 0.5f) * GraphWidth, (yPercent - 0.5f) * GraphHeight);
			return nodeTransform.localPosition;
		}

		private void _LinkNodes(Vector2 from, Vector2 to)
		{
			GameObject line = Instantiate(LinePrefab, LinesParentTransform);
			RectTransform lineTransform = ((RectTransform)line.transform);
			lineTransform.sizeDelta = new Vector2(Vector2.Distance(from, to), 2f);
			lineTransform.localPosition = from;
			float rad = Mathf.Atan((to.y - from.y) / (to.x - from.x));
			lineTransform.localEulerAngles = new Vector3(0f, 0f, rad * 180f / Mathf.PI);
		}

		#endregion
	}
}
