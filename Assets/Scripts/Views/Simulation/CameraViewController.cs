﻿using UnityEngine;
using EcoSystem.Utility;

public class CameraViewController : MonoBehaviour
{
	#region Members

	private static float MIN_CAMERA_HEIGHT = 10f;
	private static float MAX_CAMERA_HEIGHT = 55f;

	public float ZoomSpeed = 50f;
	public float PanSpeed = 2f;

	private Transform _Transform;
	private float _PreviousPinchDistance;
	private Vector3 _PreviousPanPosition;

	#endregion
	//----------------------------------------------------------------------------------------------------------------------------
	#region MonoBehaviour

	private void Start()
	{
		_Transform = gameObject.transform;
	}

	private void Update()
	{
		if (Input.GetAxis("Mouse ScrollWheel") < 0f)
		{
			_Zoom(Time.deltaTime * ZoomSpeed);
		}

		if (Input.GetAxis("Mouse ScrollWheel") > 0f)
		{
			_Zoom(-Time.deltaTime * ZoomSpeed);
		}

		if (Input.GetMouseButton(0) && Input.touchCount < 2)
		{
			Vector3 position = Input.mousePosition;
			if (_PreviousPanPosition != Vector3.zero)
			{
				_Pan(position);
			}
			_PreviousPanPosition = position;
		}
		else
		{
			_PreviousPanPosition = Vector3.zero;
		}

		//#if UNITY_ANDROID || UNITY_IPHONE || UNITY_IOS
		if (Input.touchCount.Equals(2))
		{
			float distance = Vector3.Distance(Input.touches[0].position, Input.touches[1].position);
			if (distance > _PreviousPinchDistance)
			{
				_Zoom(-Time.deltaTime * ZoomSpeed);
			}
			else if (distance < _PreviousPinchDistance)
			{
				_Zoom(Time.deltaTime * ZoomSpeed);
			}
			_PreviousPinchDistance = distance;
		}
		else
		{
			_PreviousPinchDistance = 0f;
		}
		//#endif
	}

	#endregion
	//----------------------------------------------------------------------------------------------------------------------------
	#region Private

	private void _Pan(Vector3 position)
	{
		float xMov = position.x - _PreviousPanPosition.x;
		float zMov = position.y - _PreviousPanPosition.y;
		_Transform.position -= new Vector3(xMov, 0f, zMov) * Time.deltaTime * PanSpeed;
	}

	private void _Zoom(float amount)
	{
		float height = _Transform.position.y;
		height = Mathf.Clamp(height + amount, MIN_CAMERA_HEIGHT, MAX_CAMERA_HEIGHT);
		_Transform.position = new Vector3(_Transform.position.x, height, _Transform.position.z);
	}

	#endregion
}
