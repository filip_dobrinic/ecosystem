﻿using System;
using UnityEngine;
using UnityEngine.UI;
using EcoSystem.Utility;
using EcoSystem.Models;

namespace EcoSystem.Views
{
	public class AnimalInputLabelView : MonoBehaviour
	{
		#region Members

		private const int MAX_DIGITS_NUMBER = 3;

		public AnimalType AnimalType;
		public InputField Input;

		private string _ValidatedString;

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region MonoBehaviour

		private void Start()
		{
			Input.onValueChanged.AddListener(delegate { _ValidateFieldInput(); });
		}

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region Public

		public int GetInputFieldValue()
		{
			Int32.TryParse(Input.text, out int fieldValue);
			return fieldValue;
		}

		public void SetInputFieldValue(int value)
		{
			Input.text = value == 0 ? "" : value.ToString();
		}

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region Private

		private void _ValidateFieldInput()
		{
			_ValidatedString = string.Empty;
			foreach (char c in Input.text)
			{
				if (c < Const.MIN_SINGLE_DIGIT || c > Const.MAX_SINGLE_DIGIT)
				{
					Input.text = _ValidatedString;
				}
				else
				{
					_ValidatedString += c;
				}
			}
			Input.text = Input.text.Substring(0, Math.Min(Input.text.Length, MAX_DIGITS_NUMBER));
		}
		
		#endregion
	}
}
