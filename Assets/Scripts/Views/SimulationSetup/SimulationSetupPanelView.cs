﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using EcoSystem.Controllers;
using EcoSystem.Utility;

namespace EcoSystem.Views
{
	public class SimulationSetupPanelView : MonoBehaviour
	{
		#region Members

		private const string SIMULATION_SCENE_NAME = "Scene_01";

		public List<AnimalInputLabelView> AnimalInputLabels;

		private EcoSystemController _EcoSystemController;

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region MonoBehaviour

		private void Start()
		{
			_EcoSystemController = EcoSystemController.Instance;
		}

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region Public

		public void OnGenerateButtonClick()
		{
			try
			{
				_ValidateInputs();
			}
			catch (UnityException ex)
			{
				Debug.LogWarning(ex.Message);
				return;
			}
			_FillAnimalList();
			_EcoSystemController.CreateEcoSystem();
			_BeginSimulation();
		}

		public void OnRandomizeButtonClick()
		{
			System.Random rnd = new System.Random();
			foreach (var input in AnimalInputLabels)
			{
				int randomValue = rnd.Next(Const.MIN_ANIMAL_INIT_NUMBER, Const.MAX_ANIMAL_INIT_NUMBER);
				input.SetInputFieldValue(randomValue);
			}
		}

		public void OnResetButtonClick()
		{
			foreach (var input in AnimalInputLabels)
			{
				input.SetInputFieldValue(0);
			}
		}

		#endregion
		//----------------------------------------------------------------------------------------------------------------------------
		#region Private

		private void _BeginSimulation()
		{
			SceneManager.LoadSceneAsync(SIMULATION_SCENE_NAME, LoadSceneMode.Single);
		}

		private void _FillAnimalList()
		{
			foreach (AnimalInputLabelView label in AnimalInputLabels)
			{
				for (int i = 0; i < label.GetInputFieldValue(); i++)
				{
					_EcoSystemController.AddAnimalToList(label.AnimalType);
				}
			}
		}

		private void _ValidateInputs()
		{
			string[] possibleInvalidInputs = new string[5];
			int index = 0;

			foreach (var input in AnimalInputLabels)
			{
				if (input.GetInputFieldValue() <= 0)
				{
					possibleInvalidInputs[index] = input.name;
					++index;
				}
			}
			if (index > 0)
			{
				string invalidInputs = string.Empty;
				for (int i = 0; i < index; i++)
				{
					invalidInputs += "| ";
					invalidInputs += possibleInvalidInputs[i];
					invalidInputs += " |";
				}
				throw new UnityException($"Invalid input for the following input fields: {invalidInputs}");
			}
		}
		#endregion
	}

}